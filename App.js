import React, { useState } from 'react';
import { StyleSheet, Text, View, Button, TextInput, ScrollView, FlatList } from 'react-native';

export default function App() {
  const [inputTitle, setInputTitle] = useState('')
  const [news, setNews] = useState([])

  const titleInputHandler = (text) => {
    setInputTitle(text)
  }

  const submit = () => {
    setNews([...news, inputTitle])
  }

  return (
    <View style={styles.screen}>
      <View style={styles.inputContainer}>
        <TextInput 
          placeholder="加标题" 
          style={styles.input} 
          onChangeText={titleInputHandler}
        />
        <Button title="发布" onPress={submit} />
      </View>
      <FlatList data={news} renderItem={itemData => (
        <View key={itemData.index} style={styles.newsList}>
            <Text>{itemData.item}</Text>
          </View>
      )} />
    </View>
  );
}

const styles = StyleSheet.create({
  screen: {
    padding: 50
  },
  inputContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  input: {
    borderBottomColor: 'black', 
    borderBottomWidth: 1,
    width: '80%',
    padding: 10
  },
  newsList: {
    padding: 10,
    marginVertical: 10,
    backgroundColor: '#ccc',
    borderColor: 'black',
    borderWidth: 1
  }
});
